#ifndef HWO_TRACK_H
#define HWO_TRACK_H

#include <iostream>

#include <jsoncons/json.hpp>

#include "util.h"
#include "vec2.h"
#include "hwomath.h"


class Lane
{
public:
	Lane(int index, double distanceFromCenter)
		: index(index), distanceFromCenter(distanceFromCenter)
	{}
	int index;
	double distanceFromCenter;
};

class TrackPiece
{
public:
	virtual double getLength(double ld1, double ld2) const = 0;
	virtual void calcPosition(double x1, double y1, double dir1) = 0;
	virtual void getCarPosition(double pos, double laneDistance1, double laneDistance2, double *x, double *y, double *angle = NULL) const = 0;
	virtual double getCurve(double laneDistance1, double laneDistance2);

	bool m_switch;

	double x1;
	double y1;
	double dir1;

	double x2;
	double y2;
	double dir2;
};

class Turn : public TrackPiece {
public:
	Turn(double radius, double angle, bool sw);

	virtual double getLength(double ld1, double ld2) const;
	virtual void calcPosition(double x1, double y1, double dir1);	
	virtual void getCarPosition(double pos, double laneDistance1, double laneDistance2, double *x, double *y, double *angle = NULL) const;
	virtual double getCurve(double laneDistance1, double laneDistance2);
	
	double m_radius;
	double m_angle;

	double x0;
	double y0;
};

class Straight : public TrackPiece {
public:
	Straight(double length, bool sw);
	
	virtual double getLength(double ld1, double ld2) const;
	virtual void calcPosition(double x1, double y1, double dir1);
	virtual void getCarPosition(double pos, double laneDistance1, double laneDistance2, double *x, double *y, double *angle = NULL) const;
	
	double m_length;
};

struct PathPiece
{
	PathPiece(int index, int startLane, int lane) : index(index), startLane(startLane), lane(lane) {}
	int index;
	int startLane;
	int lane; // Lane after this piece
};

struct Path
{
	Path() : length(0.0) { }
	std::vector<PathPiece> pieces;
	double length;

	// Returns lane index on the optimal path
	// -1 if path ends sooner
	int getLane(int pieceIndex) const {
		for (const PathPiece &piece : pieces) {
			if (piece.index == pieceIndex)
				return piece.lane;
		}
		return -1;
	}
};

class Track
{
public:
	Track(const jsoncons::json& definition);

	// Get the total length of the track (not taking lanes in to account)
	double getTotalLength();

	// Get distance between two points on the track
	// TODO: take lanes in to account
	double getPosDiff(const CarPosition &p1, const CarPosition &p2);
	void getCarPosition(int piece, double inPieceDistance, int lane1, int lane2, double *x, double *y, double *angle) const;

	// Returns the lane switching points for the shortest (and probably fastest) route
	Path getFastestPath(int piece, int lane, int laps=1);
	double getLaneDistance(int laneId) {
		return m_lanes[laneId]->distanceFromCenter;
	}
	double getLaneDistanceFromPieceId(const Path &path, int pieceId) {
		int laneId = path.getLane(pieceId);
		if (laneId < 0) return 0.0;
		return getLaneDistance(laneId);
	}
	//double getDistanceToCurve(int i, int lane, const Path &path);
	double getDistanceToCurve(const Path &path, const CarPosition &car, double *curve = NULL);

	double m_startX;
	double m_startY;
	double m_startAngle;

	std::vector<Lane*> m_lanes;
	std::vector<TrackPiece*> m_pieces;
};

#endif // HWO_TRACK_H
