#ifndef HWO_UTIL_H
#define HWO_UTIL_H

#include <string>
#include <vector>

struct CarId
{
	std::string name;
	std::string color;

#define FOO_OPERATOR(op) \
	bool operator op(const CarId &rhs) \
	{ \
		return this->color op rhs.color; \
	}

	FOO_OPERATOR(==);
	FOO_OPERATOR(!=);
	FOO_OPERATOR(<);
	FOO_OPERATOR(<=);
	FOO_OPERATOR(>);
	FOO_OPERATOR(>=);
#undef FOO_OPERATOR
};

struct CarPosition
{
	CarId id;
	double angle;
	int pieceIndex;
	//double piecePos;
	double inPieceDistance;
	double speed; // Needs to be calculated
	double acc; // Needs to be calculated
	double angleSpd; // Needs to be calculated
	double dToCurve; // Needs to be calculated
	double dToCurveSpd; // Needs to be calculated
	double throttle; // Needs to be calculated
	double predictSpeed; // -"-
	double predictAccuracy; // -"-
	double inPiecePerc; // -"-
	int startLane;
	int endLane;
	int lap;
};

template<class T>
class ReverseVector : public std::vector<T>
{
public:
	T &operator[](typename std::vector<T>::size_type i)
	{
		return at(this->size()-i);
	}
	const T &operator[](typename std::vector<T>::size_type i) const
	{
		return at(this->size()-i);
	}
};

#endif //HWO_UTIL_H
