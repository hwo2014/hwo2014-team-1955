#include <iostream>
#include <string>
#include <stdlib.h>
#include <getopt.h>
#include <jsoncons/json.hpp>

#include "bot.h"
#include "connection.h"
#include "protocol.h"

using namespace hwo_protocol;

int main(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"track", required_argument, NULL, 't'},
		{"password", required_argument, NULL, 'p'},
		{"cars", required_argument, NULL, 'c'},
		{"log", no_argument, NULL, 0},
		{0, 0, 0, 0}
	};

	std::map<std::string, std::string> options;
	options["track"] = "keimola";
	options["password"] = "";
	options["cars"] = "1";

	bool multi = false;

	while (true) {
		int option_index = 0;
		int c = getopt_long(argc, argv, "t:p:c:",
				long_options, &option_index);
		if (c == -1)
			break;
		switch (c) {
			case 0:
				/* If this option set a flag, do nothing else now. */
				if (long_options[option_index].flag != NULL)
					break;
				printf ("option %s", long_options[option_index].name);
				if (optarg)
					printf (" with arg %s", optarg);
				printf ("\n");
				break;
			case 't':
			case 'p':
			case 'c':
				multi = true;
				options[long_options[option_index].name] = optarg;
				break;
		}
	}

	if (optind != argc-4) {
		std::cerr << "Usage: ./run <options> host port botname botkey" << std::endl;
		return 1;
	}

	try {
		const std::string host(argv[optind]);
		const std::string port(argv[optind+1]);
		const std::string botname(argv[optind+2]);
		const std::string key(argv[optind+3]);
		std::cout << "Host: " << host << ", port: " << port << ", botname: " << botname << ", key:" << key << std::endl;

		hwo_connection connection(host, port);
		Bot bot;
		if (!multi)
			bot.run(connection, botname, key);
		else
			bot.run(connection, botname, key, options["track"], options["password"], atoi(options["cars"].c_str()));
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return 2;
	}

	return 0;
}
