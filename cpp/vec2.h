#pragma once
#ifndef CMFW_VEC2_H
#define CMFW_VEC2_H

#include <cmath>
#include <iostream>

struct vec2
{
	typedef float value_type;

	value_type x;
	value_type y;

	vec2(value_type x = 0, value_type y = 0) : x(x), y(y) {}

	template <typename T>
	vec2(const T *v)
	{
		if(v) {
			x = static_cast<value_type>(v[0]);
			y = static_cast<value_type>(v[1]);
		}
	}

	vec2 operator+(const vec2& v) const { return vec2(x + v.x, y + v.y); }
	vec2 operator-(const vec2& v) const { return vec2(x - v.x, y - v.y); }
	void operator+=(const vec2& v) { x += v.x; y += v.y; }
	void operator-=(const vec2& v) { x -= v.x; y -= v.y; }

	vec2 operator*(value_type v) const { return vec2(x*v, y*v); }
	void operator*=(value_type v) { x *= v; y *= v; }
	vec2 operator*(const vec2& v) const { return vec2(x*v.x, y*v.y); }
	void operator*=(const vec2& v) { x *= v.x; y *= v.y; }

	vec2 operator/(value_type v) const { return vec2(x/v, y/v); }
	void operator/=(value_type v) { x /= v; y /= v; }
	vec2 operator/(const vec2& v) const { return vec2(x/v.x, y/v.y); }
	void operator/=(const vec2& v) { x /= v.x; y /= v.y; }

	bool operator==(const vec2& v) const { return (x==v.x && y==v.y); }
	bool operator!=(const vec2& v) const { return (x!=v.x || y!=v.y); }

	value_type operator[](unsigned int i) const { return i==0?x:y;}
	value_type& operator[](unsigned int i) { return i==0?x:y;}

	value_type length() const
	{
		// Squaring and taking square root seriously FUCKS UP things
		// Casting float to double reduces problems when x or y are small or huge
		// WHAT HAPPENS WHEN WE ARE ALREADY USING DOUBLES?
		return (value_type)sqrt(x*(double)x + y*(double)y);
	};
	value_type lengthSq() const { return x*x + y*y; };
	void normalize()
	{
		value_type len = length();
		if (len < -0.00001 || len > 0.00001) {
			value_type il = ((value_type)1.0)/len;
			x *= il;
			y *= il;
		}
	}

	value_type min() const {return x<y?x:y;}
	value_type max() const {return x>y?x:y;}
};

vec2 normalize(const vec2 &v);

#endif // CMFW_VEC2_H
