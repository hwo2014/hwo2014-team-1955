#include "bot.h"

#include <unordered_map>

#include "protocol.h"
#include "util.h"

using namespace hwo_protocol;

Bot::Bot()
	: m_handlers {
		{ "join", &Bot::on_join },
		{ "yourCar", &Bot::on_your_car },
		{ "gameInit", &Bot::on_game_init },
		{ "gameStart", &Bot::on_game_start },
		{ "carPositions", &Bot::on_car_positions },
		{ "crash", &Bot::on_crash },
		{ "spawn", &Bot::on_spawn },
		{ "finish", &Bot::on_finish },
		{ "gameEnd", &Bot::on_game_end },
		{ "tournamentEnd", &Bot::on_tournament_end },
		{ "turboAvailable", &Bot::on_turbo_available },
		{ "error", &Bot::on_error }
	},
	m_track(NULL),
	m_vis(new Visualizer()),
	m_raceStarted(false),
	m_messageSent(false),
	m_totalLaps(0),
	m_maxAngle(60.0),
	m_id("red"),
	m_state(STATE_WAIT),
	m_data(new ReverseVector<CarPosition>()),
	m_tick(-1)
{
}

void Bot::run(hwo_connection& connection, const std::string& name, const std::string& key)
{
	m_conn = &connection;
	m_conn->send_requests({ make_join(name, key) });
	run();
}

void Bot::run(hwo_connection& connection, const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars)
{
	m_conn = &connection;
	m_conn->send_requests({ make_joinRace(name, key, track, password, cars) });
	run();
}

void Bot::run()
{
	while (m_state != STATE_END) {
		boost::system::error_code error;
		auto response = m_conn->receive_response(error);

		if (error == boost::asio::error::eof) {
			std::cout << "Connection closed" << std::endl;
			break;
		} else if (error) {
			throw boost::system::system_error(error);
		}

		handleResponse(response);
	}
}

void Bot::handleResponse(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	if (msg.has_member("gameTick")) {
		m_tick = msg["gameTick"].as<int>() + 1;
	}
	if (msg.has_member("gameId")) {
		std::string gameId = msg["gameId"].as<std::string>();
	}
	auto handler_it = m_handlers.find(msg_type);
	if (handler_it != m_handlers.end()) {
		(handler_it->second)(this, msg);
	} else {
		std::cout << "Unknown message type: " << msg_type << std::endl;
		std::cout << "Message: " << msg.to_string() << std::endl;
	}
}

void Bot::on_join(const jsoncons::json& msg)
{
	std::cout << "Joined" << std::endl;
	m_conn->send_requests({ make_ping() });
}

void Bot::on_your_car(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	m_id = data["color"].as<std::string>();
	m_vis->setCarId(m_id);
}

void Bot::on_game_init(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::cout << "Race initialized" << std::endl;

	m_track = new Track(data["race"]["track"]);
	
	m_totalLaps = data["race"]["raceSession"]["laps"].as<int>();
	std::cout << "Laps: " << m_totalLaps << "\n";

	m_conn->send_requests({ make_ping() });

	m_vis->initTrack(m_track);

	std::cout << "Race initialization complete\n";
}

void Bot::on_game_start(const jsoncons::json& msg)
{
	std::cout << "Race started" << std::endl;
	m_state = STATE_RACE;
	m_raceStarted = true;
	m_conn->send_requests({ make_throttle(1.0) });

	std::string gameId = msg["gameId"].as<std::string>();

	std::cout << "Game id: " << gameId << "\n";

	// Write game id to a file
	std::ofstream out("../gameIds", std::ofstream::app);
	out << gameId << "\n";
}

void Bot::on_car_positions(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	if (!m_raceStarted) {
		m_conn->send_requests({ make_ping() });
		return;
	}
	static int prev_pieceIndex = 0;
	static double prev_inPieceDistance = 0.0;
	static std::unordered_map<std::string, CarPosition> allPrevCarPos;

	std::unordered_map<std::string, CarPosition> allCarPos;

	for (auto iter = data.begin_elements(); iter != data.end_elements(); iter++) {
		CarPosition pos, prev;
		pos.id.name = (*iter)["id"]["name"].as<std::string>();
		pos.id.color = (*iter)["id"]["color"].as<std::string>();
		prev = allPrevCarPos[pos.id.color];
		pos.angle = (*iter)["angle"].as<double>()*M_PI/180.0;
		pos.pieceIndex = (*iter)["piecePosition"]["pieceIndex"].as<int>();
		pos.inPieceDistance = (*iter)["piecePosition"]["inPieceDistance"].as<double>();
		pos.startLane = (*iter)["piecePosition"]["lane"]["startLaneIndex"].as<int>();
		pos.endLane = (*iter)["piecePosition"]["lane"]["endLaneIndex"].as<int>();
		pos.lap = (*iter)["piecePosition"]["lap"].as<int>();

		// Calc stuff
		pos.speed = m_track->getPosDiff(prev, pos);
		pos.acc = pos.speed - prev.speed;
		pos.angleSpd = pos.angle - prev.angle;

		allCarPos[pos.id.color] = pos;
	}
	
	// TODO: choose right car
	CarPosition prev = allPrevCarPos[m_id];
	CarPosition &car = allCarPos[m_id];

	double curve = 0.0;
	car.dToCurve = m_track->getDistanceToCurve(m_path, car, &curve);
	car.dToCurveSpd = car.dToCurve - prev.dToCurve;

	double spdInCurve = 0.68 / sqrt(curve);

	double breakTime = 0.0;
	double absAngle = fabs(car.angle);
	double absAngleSpd = fabs(car.angle - prev.angle);

	car.predictSpeed = car.speed + getAcceleration(car.speed, prev.throttle);
	car.predictAccuracy = prev.predictSpeed - car.speed;

	// Do the decision
	checkLaneChange(car);

	double throttle = 0.0;

	double breakDistance = getAccelerationDistance(car.speed, spdInCurve);
	if (car.dToCurve <= breakDistance) {
		throttle = 0.0;
	} else {
		throttle = 1.0;
	}
	if (car.dToCurve < 0.0001) {
		throttle = getRequiredThrottle(car.predictSpeed, spdInCurve);
	}

	if (throttle > 1.0) throttle = 1.0;
	if (throttle < 0.0) throttle = 0.0;

	car.throttle = throttle;

	if (m_state == STATE_RACE) {

		if (fabs(car.predictAccuracy) > 0.000001) {
			std::cout << "Old speed: " << prev.speed << " " << prev.throttle << "\n";
			std::cout << "New speed: " << car.speed << " " << car.throttle << " " << car.acc << "\n";
			std::cout << "predict accuracy: " << car.predictAccuracy << "\n";
		}

		if (!m_messageSent) {
			m_conn->send_requests({ make_throttle(throttle, m_tick) });
			m_messageSent = true;
		}
	}

	m_data->push_back(car);

	m_vis->setCarPositions(allCarPos);
	m_vis->draw();
	
	allPrevCarPos = allCarPos;
	m_messageSent = false;

	//std::cout << m_data->size() << "\n";
}

void Bot::checkLaneChange(const CarPosition &car)
{
	static int foobar = -1;
	int lapsLeft = m_totalLaps - car.lap;
	if (lapsLeft > 5) lapsLeft = 5; // Limit to max 5 laps
	m_path = m_track->getFastestPath(car.pieceIndex, car.endLane, lapsLeft);
	m_vis->setPath(m_path);
	if (m_path.pieces.empty()) return;
	if (m_state != STATE_RACE) return;
	PathPiece p = m_path.pieces[0];
	int newLane = p.lane;
	int nextIndex = car.pieceIndex+1;
	if (nextIndex >= m_track->m_pieces.size()-1)
		nextIndex = 0;
	if (p.index == nextIndex && newLane != car.endLane) {
		if (!m_messageSent && newLane != foobar) {
			std::cout << "Switching lane " << car.endLane << " -> " << newLane << "\n";
			foobar = newLane;
			m_conn->send_requests({ make_switch(newLane - car.endLane, m_tick) });
			m_messageSent = true;
		}
	}
}

void Bot::on_crash(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::string name = data["name"].as<std::string>();
	std::string color = data["color"].as<std::string>();
	std::cout << name << " crashed" << std::endl;
	if (m_id.compare(color) == 0)
		m_state = STATE_CRASH;
	m_conn->send_requests({ make_ping() });
}

void Bot::on_spawn(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::string name = data["name"].as<std::string>();
	std::string color = data["color"].as<std::string>();
	std::cout << name << " spawned" << std::endl;
	if (m_id.compare(color) == 0)
		m_state = STATE_RACE;
}

void Bot::on_finish(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::string name = data["name"].as<std::string>();
	std::string color = data["color"].as<std::string>();
	std::cout << name << " finished" << std::endl;
	if (m_id.compare(color) == 0)
		m_state = STATE_WAIT;
	m_conn->send_requests({ make_ping() });
}

void Bot::on_game_end(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::cout << "Game ended" << std::endl;
	m_state = STATE_WAIT;
	m_conn->send_requests({ make_ping() });
}

void Bot::on_error(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::cerr << "Error: " << data.to_string() << std::endl;
}

void Bot::on_tournament_end(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	m_state = STATE_END;
}

void Bot::on_turbo_available(const jsoncons::json& msg)
{
	jsoncons::json data = msg["data"];
	std::cout << "Turbo available " << msg << "\n";
}

double Bot::getAcceleration(double speed, double throttle) const
{
	// TODO: remove hardcoding
	return (10*throttle - speed) / 50;
}

double Bot::getAccelerationDistance(double speed, double newSpeed) const
{
	double dist = 0.0;
	while (true) {
		dist += speed;
		if (speed <= newSpeed) break;
		speed += getAcceleration(speed, 0);
	}
	return dist;
}

double Bot::getRequiredThrottle(double speed, double desirableSpeed) const
{
	/* x2 = x + (10*y - x) / 50;
	x2 - x = (10*y - x) / 50;
	50*x2 - 50*x = 10*y - x;
	50*x2 - 49*x = 10*y;
	y = 5*x2 - 5*x + x/10 */
	return 5*desirableSpeed - 4.9*speed;
}
