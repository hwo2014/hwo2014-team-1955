#include "visualizer.h"

#include <cstdio>
#include <limits>

#ifdef ENABLE_VISUALIZATION
#include <GLFW/glfw3.h>
#endif

#include "../vec2.h"

struct VisualizerPrivate
{
#ifdef ENABLE_VISUALIZATION
	bool error = false;
	GLFWwindow* window = NULL;
	const Track *track = NULL;
	std::unordered_map<std::string, CarPosition> carPos;
	std::unordered_map<std::string, CarPosition> prevCarPos;
	Path path;

	double minX = std::numeric_limits<double>::max();
	double minY = std::numeric_limits<double>::max();
	double maxX = std::numeric_limits<double>::min();
	double maxY = std::numeric_limits<double>::min();

	std::string id = "red";
	double carWidth = 20.0;
	double carLength = 40.0;
	double guideFlagPos = 10.0;
	std::unordered_map<std::string, std::vector<float> > colors;
	ReverseVector<CarPosition> *data;
#endif
};

Visualizer::Visualizer()
	: d(new VisualizerPrivate)
{
#ifdef ENABLE_VISUALIZATION
	printf("Visualizer initialized.\n");

	if (!glfwInit()) {
		fprintf(stderr, "Could not init visualizer.\n");
		d->error = true;
		return;
	}

	glfwWindowHint(GLFW_SAMPLES, 16);
	glfwWindowHint(GLFW_RESIZABLE, 0);

	d->window = glfwCreateWindow(640, 640, "Hello World Open Visualizer", NULL, NULL);
	if (!d->window) {
		fprintf(stderr, "Could not create a window.\n");
		d->error = true;
		return;
	}

	glViewport(0, 0, 640, 640);

	glfwMakeContextCurrent(d->window);

	d->colors["red"] = {1.0f, 0.0f, 0.0f};
	d->colors["green"] = {0.0f, 1.0f, 0.0f};
	d->colors["blue"] = {0.0f, 0.0f, 1.0f};
	d->colors["yellow"] = {1.0f, 1.0f, 0.0f};
	d->colors["purple"] = {1.0f, 0.0f, 1.0f};
	d->colors["teal"] = {0.0f, 1.0f, 1.0f};
#endif
}

Visualizer::~Visualizer()
{
#ifdef ENABLE_VISUALIZATION
	printf("Visualizer destroyed.\n");
	delete d;
#endif
}

void Visualizer::initTrack(const Track *track)
{
#ifdef ENABLE_VISUALIZATION
	d->track = track;
#endif
}

void Visualizer::initCarDimensions(double width, double length, double guideFlagPos)
{
#ifdef ENABLE_VISUALIZATION
	d->carWidth = width;
	d->carLength = length;
	d->guideFlagPos = guideFlagPos;
#endif
}

void Visualizer::setCarId(const std::string id)
{
#ifdef ENABLE_VISUALIZATION
	d->id = id;
#endif
}

void Visualizer::setCarPositions(const std::unordered_map<std::string, CarPosition> &cars)
{
#ifdef ENABLE_VISUALIZATION
	d->prevCarPos = d->carPos;
	d->carPos = cars;
#endif
}

void Visualizer::setData(ReverseVector<CarPosition> *data)
{
#ifdef ENABLE_VISUALIZATION
	d->data = data;
#endif
}

void Visualizer::setPath(const Path &path)
{
#ifdef ENABLE_VISUALIZATION
	d->path = path;
#endif
}

void Visualizer::draw()
{
#ifdef ENABLE_VISUALIZATION
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	if (!d->track) return;
	double x = d->track->m_startX;
	double y = d->track->m_startY;
	double dir = d->track->m_startAngle;

	double x1 = x;
	double y1 = y;
	double dir1 = dir;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(d->minX-50, d->maxX+50, d->maxY+50, d->minY-50, 0.0, 1.0);
	glOrtho(0.0, 1.0, 1.0, 0.0, 0.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	double scale = std::max(d->maxX - d->minX, d->maxY - d->minY);
	scale = 0.9/scale;
	glScalef(scale, scale, scale);
	glTranslatef(-d->minX+50, -d->minY+50, 0.0f);
	//std::cout << "scale: " << scale << "\n";

	glColor3f(1.0f, 1.0f, 1.0f);

	for (auto &p : d->track->m_pieces) {
		double x1 = p->x1;
		double x2 = p->x2;
		double y1 = p->y1;
		double y2 = p->y2;

		if (x2 > d->maxX) d->maxX = x2;
		if (x2 < d->minX) d->minX = x2;
		if (y2 > d->maxY) d->maxY = y2;
		if (y2 < d->minY) d->minY = y2;

		int sub_pieces = 0;

		glBegin(GL_LINES);
		if (dynamic_cast<Straight*>(p)) {
			sub_pieces = 1;
		} else if (dynamic_cast<Turn*>(p)){
			sub_pieces = 8;
		}
		glColor3f(1.0f, 1.0f, 1.0f);
		if (p->m_switch) {
			glColor3f(0.0f, 1.0f, 0.0f);
		}
		for (int i=0; i<sub_pieces; i++) {
			//for (Lane *lane : d->track->m_lanes) {
			for (int j=0; j<d->track->m_lanes.size(); j++) {
				Lane *lane = d->track->m_lanes[j];
				double x1, y1, x2, y2, angle;
				double laneDist = lane->distanceFromCenter;
				p->getCarPosition(i/(double)(sub_pieces), laneDist, laneDist, &x1, &y1);
				p->getCarPosition((i+1)/(double)(sub_pieces), laneDist, laneDist, &x2, &y2);
				glVertex2f(x1, y1);
				glVertex2f(x2, y2);

				if (p->m_switch && j < d->track->m_lanes.size() - 1) {
					Lane *lane2 = d->track->m_lanes[j+1];
					double ld1 = lane->distanceFromCenter;
					double ld2 = lane2->distanceFromCenter;

					p->getCarPosition(i/(double)(sub_pieces), ld1, ld2, &x1, &y1);
					p->getCarPosition((i+1)/(double)(sub_pieces), ld1, ld2, &x2, &y2);
					glVertex2f(x1, y1);
					glVertex2f(x2, y2);

					p->getCarPosition(i/(double)(sub_pieces), ld2, ld1, &x1, &y1);
					p->getCarPosition((i+1)/(double)(sub_pieces), ld2, ld1, &x2, &y2);
					glVertex2f(x1, y1);
					glVertex2f(x2, y2);
				}
			}
		}
		glEnd();
	}

	// Draw cars
	//for (CarPosition &pos : d->carPos) {
	for (auto &iter : d->carPos) {
		CarPosition pos = iter.second;
		double x = 0.0;
		double y = 0.0;
		double angle = 0.0;
		d->track->getCarPosition(pos.pieceIndex, pos.inPieceDistance, pos.startLane, pos.endLane, &x, &y, &angle);
		//std::cout << "Drawing a car at " << x << " " << y << " " << angle << "\n";

		glPushMatrix();
		glTranslatef(x, y, 0.0f);
		glRotatef((angle+pos.angle)*180.0/M_PI - 90.0, 0.0f, 0.0f, 1.0f);

		if (d->colors.find(pos.id.color) != d->colors.end())
			glColor3fv(d->colors[pos.id.color].data());
		else
			glColor3f(1.0f, 1.0f, 1.0f);

		glBegin(GL_QUADS);
		glVertex2f(-d->carWidth/2.0, d->guideFlagPos);
		glVertex2f(d->carWidth/2.0, d->guideFlagPos);
		glVertex2f(d->carWidth/2.0, -(d->carLength - d->guideFlagPos));
		glVertex2f(-d->carWidth/2.0, -(d->carLength - d->guideFlagPos));
		glEnd();

		glPopMatrix();
	}

	// Draw path
	glColor3f(0.0f, 1.0f, 1.0f);
	glBegin(GL_LINE_STRIP);
	if (!d->carPos.empty()) {
		const CarPosition &car = d->carPos[d->id];
		for (int i=0; i<d->path.pieces.size() && i < 10; i++) {
			PathPiece pp = d->path.pieces[i];
			TrackPiece *piece = d->track->m_pieces[pp.index];
			double ld = d->track->m_lanes[pp.lane]->distanceFromCenter;
			double x, y, angle;
			d->track->getCarPosition(pp.index, 0.01, pp.startLane, pp.lane, &x, &y, &angle);
			glVertex2f(x, y);
		}
	}
	glEnd();

	glLoadIdentity();

	CarPosition self = d->carPos[d->id];
	CarPosition prev = d->prevCarPos[d->id];

	glColor3f(0.0f, 1.0f, 0.0f);
	drawBar(0, self.speed/20.0f);
	drawBar(1, self.dToCurveSpd/-20.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	drawBar(2, self.acc, 0.5);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawBar(3, self.angle*180.0/M_PI/120.0, 0.5);
	glColor3f(0.0f, 1.0f, 0.0f);
	drawBar(4, self.angleSpd*5.0, 0.5);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawBar(5, self.inPiecePerc/100.0f);
	drawBar(6, self.dToCurve/500.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	drawBar(7, self.throttle);
	glColor3f(1.0f, 0.0f, 0.0f);
	drawBar(8, self.predictAccuracy*100, 0.5);

	glBegin(GL_LINES);
	glVertex2f(0.5f, 1.0f);
	glVertex2f(0.5f, 1.0f-0.01f*4);
	glEnd();


	glfwSwapBuffers(d->window);
	//glfwPollEvents();
#endif
}

void Visualizer::drawBar(int index, double value, double center)
{
#ifdef ENABLE_VISUALIZATION
	glBegin(GL_QUADS);
	glVertex2f(center, 1.0f-index*0.01f);
	glVertex2f(center + value, 1.0f-index*0.01f);
	glVertex2f(center + value, 1.0f-(index+1)*0.01f);
	glVertex2f(center, 1.0f-(index+1)*0.01f);
	glEnd();
#endif
}
