#pragma once

#include <vector>
#include <unordered_map>

#include "../util.h"
#include "../track.h"

struct VisualizerPrivate;
class Visualizer
{
public:
	Visualizer();

	void initTrack(const Track *track);
	void initCarDimensions(double width, double length, double guideFlagPos);

	void setCarId(const std::string id);
	void setCarPositions(const std::unordered_map<std::string, CarPosition> &cars);
	void setPath(const Path &path);
	void setData(ReverseVector<CarPosition> *data);
	void draw();

	~Visualizer();
private:
	VisualizerPrivate * const d;

	void drawBar(int index, double value, double center=0.0);
};
