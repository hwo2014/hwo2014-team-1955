#include "track.h"

#include <algorithm>

double TrackPiece::getCurve(double laneDistance1, double laneDistance2)
{
	return 0.0;
}

Straight::Straight(double length, bool sw)
	: m_length(length)
{
	m_switch = sw;
}

double Straight::getLength(double ld1, double ld2) const
{
	double a = m_length;
	double b = ld2-ld1;
	return sqrt(a*a + b*b);
}
	
void Straight::calcPosition(double x1, double y1, double dir1)
{
	this->x1 = x1;
	this->y1 = y1;
	this->dir1 = dir1;
	x2 = x1 + cos(dir1) * m_length;
	y2 = y1 + sin(dir1) * m_length;
	dir2 = dir1;
}

void Straight::getCarPosition(double pos, double laneDistance1, double laneDistance2, double *x, double *y, double *angle) const
{
	double laneDistance = interpolateHermite(laneDistance1, laneDistance2, pos);
	*x = x1 + (x2-x1)*pos + cos(dir1+M_PI/2.0)*laneDistance;
	*y = y1 + (y2-y1)*pos + sin(dir1+M_PI/2.0)*laneDistance;
	if (angle) *angle = interpolate(dir1, dir2, pos);
}

Turn::Turn(double radius, double angle, bool sw)
	: m_radius(radius), m_angle(angle)
{
	m_switch = sw;
}

double Turn::getLength(double ld1, double ld2) const
{
	// FAR FROM PERFECT
	double ld = (ld1+ld2)/2.0;
	if (m_angle > 0.0) ld = -ld;
	return fabs(m_angle) * (m_radius + ld);
}

void Turn::calcPosition(double x1, double y1, double dir1)
{
	this->x1 = x1;
	this->y1 = y1;
	this->dir1 = dir1;
	double dirToOrigo = dir1 + sgn(m_angle)*M_PI/2.0;
	double dirTo2 = dirToOrigo + M_PI + m_angle;
	x0 = x1 + cos(dirToOrigo) * m_radius;
	y0 = y1 + sin(dirToOrigo) * m_radius;
	x2 = x0 + cos(dirTo2) * m_radius;
	y2 = y0 + sin(dirTo2) * m_radius;
	dir2 = dir1 + m_angle;
}
	
void Turn::getCarPosition(double pos, double laneDistance1, double laneDistance2, double *x, double *y, double *angle) const
{
	double dirToOrigo = dir1 + sgn(m_angle)*M_PI/2.0;
	double lanePos = interpolateHermite(laneDistance1, laneDistance2, pos);
	double dirTo2 = dirToOrigo + M_PI + m_angle*pos;
	if (m_angle > 0.0) lanePos = -lanePos;
	*x = x0 + cos(dirTo2) * (m_radius + lanePos);
	*y = y0 + sin(dirTo2) * (m_radius + lanePos);
	if (angle) *angle = interpolate(dir1, dir2, pos);
}

double Turn::getCurve(double ld1, double ld2)
{
	float offset = (ld1 + ld2) / 2.0;

	if (m_angle > 0)
		offset = -offset;
	return 1.0 / (m_radius + offset);
}

Track::Track(const jsoncons::json& definition)
{
	int piece_count = definition["pieces"].size();
	int lane_count = definition["lanes"].size();

	std::cout << "Track: " << definition["name"].as<std::string>() << std::endl;
	std::cout << "Pieces: " << piece_count << std::endl;
	std::cout << "Lanes: " << lane_count << std::endl;

	double x = definition["startingPoint"]["position"]["x"].as<double>();
	double y = definition["startingPoint"]["position"]["y"].as<double>();
	double dir = definition["startingPoint"]["angle"].as<double>() * M_PI/180 - M_PI/2.0;

	for (auto iter = definition["pieces"].begin_elements(); iter != definition["pieces"].end_elements(); iter++) {
		bool has_switch = iter->has_member("switch");
		TrackPiece *piece;
		if (iter->has_member("length")) {
			// It is a straight
			double length = (*iter)["length"].as<double>();
			//std::cout << "L: " << length << "\n";
			piece = new Straight(length, has_switch);
		} else {
			// It is a turn
			double radius = (*iter)["radius"].as<double>();
			double angle = (*iter)["angle"].as<double>() * M_PI/180.0;
			//std::cout << "A: " << angle << "\n";
			piece = new Turn(radius, angle, has_switch);
		}
		piece->calcPosition(x, y, dir);
		m_pieces.push_back(piece);

		x = piece->x2;
		y = piece->y2;
		dir = piece->dir2;
	}
	std::cout << "Total length: " << getTotalLength() << std::endl;

	// Get & set starting position
	m_startX = definition["startingPoint"]["position"]["x"].as<double>();
	m_startY = definition["startingPoint"]["position"]["y"].as<double>();
	m_startAngle = definition["startingPoint"]["angle"].as<double>() * M_PI/18.0;

	std::cout << "Start: x=" << m_startX << " y=" << m_startY << " angle=" << m_startAngle << "\n";

	// Get & set lanes
	for (auto iter = definition["lanes"].begin_elements(); iter != definition["lanes"].end_elements(); iter++) {
		Lane *lane = new Lane((*iter)["index"].as<int>(), (*iter)["distanceFromCenter"].as<double>());
		m_lanes.push_back(lane);
	}
}

double Track::getTotalLength()
{
	double total = 0.0;
	for (std::vector<TrackPiece*>::iterator iter = m_pieces.begin(); iter != m_pieces.end(); iter++) {
		double len = (*iter)->getLength(0.0, 0.0);
		total += len;
	}
	return total;
}

//double Track::getPosDiff(int id1, double pos1, int id2, double pos2)
double Track::getPosDiff(const CarPosition &p1, const CarPosition &p2)
{
	if (p1.pieceIndex == p2.pieceIndex)
		return p2.inPieceDistance - p1.inPieceDistance;
	else {
		double p1len = m_pieces[p1.pieceIndex]->getLength(m_lanes[p1.startLane]->distanceFromCenter, m_lanes[p1.endLane]->distanceFromCenter);
		double p2len = m_pieces[p2.pieceIndex]->getLength(m_lanes[p2.startLane]->distanceFromCenter, m_lanes[p2.endLane]->distanceFromCenter);
		/*std::cout << "prev piece length: " << p1len << "\n";
		std::cout << "curr piece length: " << p2len <<"\n";
		std::cout << "ipd: " << p1.inPieceDistance << " " << p2.inPieceDistance << "\n";*/
		return p1len - p1.inPieceDistance
			+ p2.inPieceDistance;
	}
}

void Track::getCarPosition(int pieceIndex, double inPieceDistance, int lane1, int lane2, double *x, double *y, double *angle) const
{
	TrackPiece *piece = m_pieces[pieceIndex];
	double piecePos = inPieceDistance / piece->getLength(m_lanes[lane1]->distanceFromCenter, m_lanes[lane2]->distanceFromCenter); // NOTE: may not be accurate when changing lanes in turns
	piece->getCarPosition(piecePos, m_lanes[lane1]->distanceFromCenter, m_lanes[lane2]->distanceFromCenter, x, y, angle);
}

Path Track::getFastestPath(int pos, int lane, int laps)
{
	if (laps <= 0) return Path();
	int lanes = m_lanes.size();
	Path *oldPath = new Path[lanes];
	Path *path = new Path[lanes];
	for (int i=0; i<lanes; i++) {
		oldPath[i] = Path();
		path[i] = Path();
	}

	int i=0;

	while (true) {
		TrackPiece *piece = m_pieces[i];

		for (int j=0; j<lanes; j++) {
			if (piece->m_switch) {
				// Select no switch as initial choice
				// If another route is as long, no switch is preferred
				double minLength = oldPath[j].length;
				int minIndex = j;
				if (j > 0 && oldPath[j-1].length < minLength) {
					minLength = oldPath[j-1].length;
					minIndex = j-1;
				}
				if (j < lanes-1 && oldPath[j+1].length < minLength) {
					minLength = oldPath[j+1].length;
					minIndex = j+1;
				}
				path[j] = oldPath[minIndex];
				path[j].pieces.push_back(PathPiece(i, j, minIndex));
			} else {
				path[j].pieces.push_back(PathPiece(i, j, j));
			}
		}

		for (int j=0; j<lanes; j++) {
			double length = piece->getLength(m_lanes[j]->distanceFromCenter, m_lanes[j]->distanceFromCenter);
			path[j].length += length;
		}

		--i;
		if (i < 0)
			i = m_pieces.size()-1;

		if (i == pos) {
			--laps;
			if (laps == 0)
				break;
		}

		for (int j=0; j<lanes; j++) {
			oldPath[j] = path[j];
		}
	}

	Path ret = path[lane];

	delete[] oldPath;
	delete[] path;

	std::reverse(ret.pieces.begin(), ret.pieces.end());

	return ret;
}

//double getDistanceToCurve(int i, int lane, const Path &path);
double Track::getDistanceToCurve(const Path &path, const CarPosition &car, double *curve)
{
	int i = car.pieceIndex;
	double c = 0.0;
	double dToCurve = 0.0;
	while (true) {
		// Start lane distance (currently end lane...)
		double ld1 = getLaneDistanceFromPieceId(path, i);
		// End lane distance
		//double ld2 = m_track->getLaneDistanceFromPieceId(m_path, (i+1)%m_track->m_pieces.size());
		c = m_pieces[i]->getCurve(ld1, ld1);
		if (c != 0.0) break;
		i = (++i) % m_pieces.size();
		dToCurve += m_pieces[i]->getLength(ld1, ld1);
	}
	dToCurve -= car.inPieceDistance;
	if (dToCurve < 0.0)
		dToCurve = 0.0;
	if (curve) *curve = c;
	return dToCurve;
}
