#include "protocol.h"

namespace hwo_protocol
{

jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data, int gameTick)
{
	jsoncons::json r;
	r["msgType"] = msg_type;
	r["data"] = data;
	if (gameTick >= 0)
		r["gameTick"] = gameTick;
	return r;
}

jsoncons::json make_join(const std::string& name, const std::string& key)
{
	jsoncons::json data;
	data["name"] = name;
	data["key"] = key;
	return make_request("join", data);
}

jsoncons::json make_joinRace(const std::string& name, const std::string& key, int cars)
{
	jsoncons::json data;
	jsoncons::json botId;
	botId["name"] = name;
	botId["key"] = key;
	data["botId"] = botId;
	data["carCount"] = cars;
	return make_request("joinRace", data);
}

jsoncons::json make_joinRace(const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars)
{
	jsoncons::json data;
	jsoncons::json botId;
	botId["name"] = name;
	botId["key"] = key;
	data["botId"] = botId;
	data["trackName"] = track;
	data["password"] = password;
	data["carCount"] = cars;
	return make_request("joinRace", data);
}

jsoncons::json make_race(const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars)
{
	jsoncons::json data;
	jsoncons::json botId;
	botId["name"] = name;
	botId["key"] = key;
	data["botId"] = botId;
	data["trackName"] = track;
	data["password"] = password;
	data["carCount"] = cars;
	return make_request("createRace", data);
}

jsoncons::json make_ping()
{
	return make_request("ping", jsoncons::null_type());
}

jsoncons::json make_throttle(double throttle, int gameTick)
{
	return make_request("throttle", throttle, gameTick);
}

jsoncons::json make_switch(int dir, int gameTick)
{
	jsoncons::json r;
	r["msgType"] = "switchLane";
	if (dir < 0)
		r["data"] = "Left";
	if (dir > 0)
		r["data"] = "Right";
	if (gameTick >= 0)
		r["gameTick"] = gameTick;

	return r;
}

jsoncons::json make_useTurbo(int gameTick)
{
	jsoncons::json r;
	r["msgType"] = "turbo";
	r["data"] = "All your prices are belong to us!";
	if (gameTick >= 0)
		r["gameTick"] = gameTick;
	return r;
}

}  // namespace hwo_protocol
