#include "vec2.h"

vec2 normalize(const vec2 &v) {
	vec2 ret = v;
	ret.normalize();
	return ret;
}
