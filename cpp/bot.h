#ifndef HWO_BOT_H
#define HWO_BOT_H

#include <string>
#include <unordered_map>

#include "connection.h"
#include "track.h"
#include "visualizer/visualizer.h"

class Bot
{
public:
	Bot();

	void run(hwo_connection& connection, const std::string& name, const std::string& key);
	void run(hwo_connection& connection, const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars);

private:
	typedef std::function<void(Bot*, const jsoncons::json&)> handler_fun;
	enum BotState { STATE_WAIT, STATE_RACE, STATE_CRASH, STATE_END};

	const std::unordered_map<std::string, handler_fun> m_handlers;
	hwo_connection *m_conn;
	Track *m_track;
	Visualizer *m_vis;

	std::string m_id;

	bool m_raceStarted;
	bool m_messageSent;
	BotState m_state;

	int m_totalLaps;

	double m_maxAngle;
	Path m_path;
	int m_tick;

	ReverseVector<CarPosition> *m_data;

	void handleResponse(const jsoncons::json& msg);

	void on_join(const jsoncons::json& data);
	void on_your_car(const jsoncons::json& data);
	void on_game_init(const jsoncons::json& data);
	void on_game_start(const jsoncons::json& data);
	void on_finish(const jsoncons::json& data);
	void on_car_positions(const jsoncons::json& data);
	void on_crash(const jsoncons::json& data);
	void on_spawn(const jsoncons::json& data);
	void on_game_end(const jsoncons::json& data);
	void on_error(const jsoncons::json& data);
	void on_tournament_end(const jsoncons::json& data);
	void on_turbo_available(const jsoncons::json& data);

	// Switches to a faster lane if possible
	void checkLaneChange(const CarPosition &car);

	double getAcceleration(double speed, double throttle) const;
	// Works for bot acceleration and deceleration
	double getAccelerationDistance(double speed, double newSpeed) const;
	double getRequiredThrottle(double speed, double desirableSpeed) const;

	void run();
};

#endif // HWO_BOT_H
