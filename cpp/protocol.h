#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace hwo_protocol
{
jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data, int gameTick=-1);
jsoncons::json make_join(const std::string& name, const std::string& key);
jsoncons::json make_joinRace(const std::string& name, const std::string& key, int cars);
jsoncons::json make_joinRace(const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars);
jsoncons::json make_race(const std::string& name, const std::string& key, const std::string &track, const std::string &password, int cars);
jsoncons::json make_ping();

jsoncons::json make_throttle(double throttle, int gameTick = -1);
jsoncons::json make_switch(int dir, int gameTick = -1);
jsoncons::json make_useTurbo(int gameTick = -1);
}

#endif
