#pragma once
#ifndef HWO_MATH_H
#define HWO_MATH_H

template<typename T, typename T2>
T interpolate(const T &x0, const T &x1, const T2 &val)
{
	return x0 + (x1-x0) * val;
}

template<typename T, typename T2>
T interpolateHermite(const T &x0, const T &x1, const T2 &val)
{
	T2 t = (3.0f * val * val) - (2.0f * val * val * val);
	return x0 + (x1-x0) * t;
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

#endif // HWO_MATH_H
