﻿using Newtonsoft.Json;

public class MsgWrapper
{
	public string msgType;
	public object data;
	//public string gameId;
	//public int gameTick;

	public MsgWrapper(string msgType, object data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}

public abstract class SendMsg
{
	public string ToJson()
	{
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual object MsgData()
	{
		return this;
	}

	protected abstract string MsgType();
}

public class Join : SendMsg
{
	public string name;
	public string key;

	public Join(string name, string key)
	{
		this.name = name;
		this.key = key;
	}

	protected override string MsgType()
	{
		return "join";
	}
}

public class CreateRace : SendMsg
{
	public PlayerIDData botId;
	public string trackName;
	public string password;
	public int carCount;

	public CreateRace(string name, string key, string trackName, string password, int carCount)
	{
		this.botId = new PlayerIDData();
		this.botId.name = name;
		this.botId.key = key;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType()
	{
		return "createRace";
	}
}

public class JoinRace : SendMsg
{
	public PlayerIDData botId;
	public string trackName;
	public string password;
	public int carCount;

	public JoinRace(string name, string key, string trackName, string password, int carCount)
	{
		this.botId = new PlayerIDData();
		this.botId.name = name;
		this.botId.key = key;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType()
	{
		return "joinRace";
	}
}

public class Ping : SendMsg
{
	protected override string MsgType()
	{
		return "ping";
	}
}

public class Throttle : SendMsg
{
	public double value;

	public Throttle(double value)
	{
		this.value = value;
	}

	protected override object MsgData()
	{
		return this.value;
	}

	protected override string MsgType()
	{
		return "throttle";
	}
}

public class Turbo : SendMsg
{
	string msg;
	
	public Turbo(string msg)
	{
		this.msg = msg;
	}

	protected override object MsgData()
	{
		return msg;
	}

	protected override string MsgType()
	{
		return "turbo";
	}
}

public class SwitchLane : SendMsg
{
	public string lane;
	public SwitchLane(string lane)
	{
		this.lane = lane;
	}

	protected override object MsgData()
	{
		return this.lane;
	}

	protected override string MsgType()
	{
		return "switchLane";
	}
}

public class GameInitData
{
	public RaceData race;
}

public class RaceData
{
	public TrackData track;
	public CarData[] cars;
	public RaceSessionData raceSession;
}

public class RaceSessionData
{
	public int laps;
	public int maxLapTimeMs;
	public bool quickRace;
}

public class TrackData
{
	public string id;
	public string name;
	public PieceData[] pieces;
	public LaneData[] lanes;
	public StartingPointData startingPoint;
}

public class PieceData
{
	public float length;
	[JsonProperty("switch")]
	public bool switch_;
	public int radius;
	public float angle;
}

public class LaneData
{
	public int distanceFromCenter;
	public int index;
}

public class StartingPointData
{
	public PositionData position;
	public float angle;
}

public class PositionData
{
	public float x;
	public float y;
}

public class CarData
{
	public PlayerIDData id;
	public CarDimensionsData dimensions;
}

public class CarDimensionsData
{
	public float length;
	public float width;
	public float guideFlagPosition;
}

public class CarPositionData
{
	public PlayerData id;
	public float angle;
	public PiecePositionData piecePosition;
}

public class PlayerIDData
{
	public string name;
	public string key;
}

public class PlayerData
{
	public string name;
	public string color;
}

public class PiecePositionData
{
	public int pieceIndex;
	public float inPieceDistance;
	public LanePositionData lane;
	public int lap;
}
public class LanePositionData
{
	public int startLaneIndex;
	public int endLaneIndex;
}

public class TurboData
{
	public float turboDurationMilliseconds;
	public int turboDurationTicks;
	public float turboFactor;
}