﻿using System;
using System.Collections.Generic;

public class CarParameters
{

	public abstract class Input
	{
		public abstract float Compare(Input other);
	}

	public abstract class Output
	{
	}
	
	public abstract class Parameter<TIn, TOut>
	{
		public TIn input;
		public TOut output;
	}

	public class SpeedInput : Input
	{
		public float throttle;
		public float speed;

		public override float Compare(Input other)
		{
			SpeedInput input = (SpeedInput)other;
			float dt = Math.Abs(throttle - input.throttle) * 10;
			float ds = Math.Abs(speed - input.speed);
			return dt + ds;
		}
	}
	public class SpeedOutput : Output
	{
		public float speed;
	}

	public class SpeedParam : Parameter<SpeedInput, SpeedOutput>
	{
	}

	public class PositionInput : Input
	{
		public int pieceIndex;
		public float inPiecePosition;
		public float speed;

		public override float Compare(Input other)
		{
			PositionInput input = (PositionInput)other;
			float d1 = pieceIndex == input.pieceIndex ? 0 : 100;
			float d2 = Math.Abs(inPiecePosition - input.inPiecePosition) * .1f;
			float d3 = Math.Abs(speed - input.speed);
			return d1 + d2 + d3;
		}
	}
	public class PositionOutput : Output
	{
		public int pieceIndex;
		public float inPiecePosition;
	}

	public class PositionParam : Parameter<PositionInput, PositionOutput>
	{
	}

	public class AngleInput : Input
	{
		public float pieceCurve;
		public float speed;
		public float angle;
		public float angleSpd;

		public override float Compare(Input other)
		{
			AngleInput input = (AngleInput)other;
			float d1 = Math.Abs(pieceCurve - input.pieceCurve) * 100;
			float d2 = Math.Abs(speed - input.speed);
			float d3 = Math.Abs(angle - input.angle) / 5;
			float d4 = Math.Abs(angleSpd - input.angleSpd);
			return d1 + d2 + d3 + d4;
		}
	}

	public class AngleOutput : Output
	{
		public float angle;
		public float angleSpd;
	}

	public class AngleParam : Parameter<AngleInput, AngleOutput>
	{
	}

	public List<SpeedParam> m_speedData = new List<SpeedParam>();
	public List<PositionParam> m_positionData = new List<PositionParam>();
	public List<AngleParam> m_angleData = new List<AngleParam>();

	public float crashAngle = 60;

	static float CompareThreshold = 0.1f;

	public void recordSpeed(SpeedInput input, SpeedOutput output)
	{
		recordParam<SpeedParam, SpeedInput, SpeedOutput>(input, output, m_speedData);
	}
	public void recordPosition(PositionInput input, PositionOutput output)
	{
		recordParam<PositionParam, PositionInput, PositionOutput>(input, output, m_positionData);
	}
	public void recordAngle(AngleInput input, AngleOutput output)
	{
		recordParam<AngleParam, AngleInput, AngleOutput>(input, output, m_angleData);
	}

	public SpeedOutput getSpeed(SpeedInput input, out float error)
	{
		return getOutput<SpeedParam, SpeedInput, SpeedOutput>(input, m_speedData, out error);
	}
	public PositionOutput getPosition(PositionInput input, out float error)
	{
		return getOutput<PositionParam, PositionInput, PositionOutput>(input, m_positionData, out error);
	}
	public AngleOutput getAngle(AngleInput input, out float error)
	{
		return getOutput<AngleParam, AngleInput, AngleOutput>(input, m_angleData, out error);
	}

	private void recordParam<T, TIn, TOut>(TIn input, TOut output, List<T> list) where T : Parameter<TIn, TOut>, new() where TIn : Input where TOut : Output
	{
		// if there is already a record close to this input, update the output
		foreach (Parameter<TIn, TOut> p in list)
		{
			float d = p.input.Compare(input);
			if (d < CompareThreshold)
			{
				p.input = input;
				p.output = output;
				return;
			}
		}

		// else add a new record
		Console.Write("New record " + input.GetType() + " ");
		T param = new T();
		param.input = input;
		param.output = output;
		list.Add(param);
	}

	private TOut getOutput<T, TIn, TOut>(TIn input, List<T> list, out float error) where T : Parameter<TIn, TOut> where TIn : Input where TOut : Output
	{
		TOut closest = null;
		float closestD = float.MaxValue;
		foreach (T p in list)
		{
			float d = p.input.Compare(input);
			if (d < closestD)
			{
				closestD = d;
				closest = p.output;
				if (d < CompareThreshold)
					break;
			}
		}
		error = closestD / CompareThreshold;
		return closest;
	}
}