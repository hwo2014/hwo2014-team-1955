using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

public class LearnBot : Bot
{
	
	#region PUBLIC FUNCTIONS

	public LearnBot()
	{
		m_params = new CarParameters();
	}
	
	public void loadData(string path)
	{
		Console.WriteLine("Loading from " + path);
		try
		{
			StreamReader sr = new StreamReader(path);
			string str = sr.ReadToEnd();
			sr.Close();
			m_params = JsonConvert.DeserializeObject<CarParameters>(str);
		}
		catch(System.Exception e)
		{
			Console.WriteLine("Loading error: " + e.ToString());
		}
	}

	public void saveData(string path)
	{
		Console.WriteLine("Saving to " + path);
		string str = JsonConvert.SerializeObject(m_params);
		StreamWriter sw = new StreamWriter(path);
		sw.Write(str);
		sw.Close();
	}

	#endregion
	
	#region BOT OVERRIDES
	
	protected override void carPositions(CarPositionData[] carPositions)
	{
		if (m_crashed)
		{
			m_speedInput = null;
			m_positionInput = null;
			m_angleInput = null;
			send(new Ping());
			return;
		}


		CarPositionData myPosData = null;

		foreach (CarPositionData cpd in carPositions)
			if (cpd.id.name == m_myName)
				myPosData = cpd;

		int pieceID = myPosData.piecePosition.pieceIndex;

		PieceData thisPiece = m_raceData.track.pieces[pieceID];

		int lane = myPosData.piecePosition.lane.startLaneIndex;
				
		float piecePos = myPosData.piecePosition.inPieceDistance;
		if (piecePos > m_lastPos)
			m_speed = piecePos - m_lastPos;
		m_lastPos = piecePos;

		m_angleSpd = myPosData.angle - m_lastAngle;
		m_lastAngle = myPosData.angle;

		float throttle = getThrottle(myPosData);

		Console.WriteLine("gameTick: " + m_gameTick + " throttle: " + throttle.ToString("0.00") + " speed: " + m_speed.ToString("0.00") + " piece: " + pieceID + " crashAngle: " + m_params.crashAngle.ToString("0.00") + " lane: " + lane);
		//Console.WriteLine("speed: " + m_speed);
		//Console.WriteLine("piece: " + pieceID);
		//Console.WriteLine("crashAngle: " + m_params.crashAngle);

		if (m_speedInput != null)
		{
			CarParameters.SpeedOutput speedOutput = new CarParameters.SpeedOutput();
			speedOutput.speed = m_speed;

			CarParameters.PositionOutput positionOutput = new CarParameters.PositionOutput();
			positionOutput.inPiecePosition = myPosData.piecePosition.inPieceDistance;
			positionOutput.pieceIndex = myPosData.piecePosition.pieceIndex;

			CarParameters.AngleOutput angleOutput = new CarParameters.AngleOutput();
			angleOutput.angle = myPosData.angle;
			angleOutput.angleSpd = m_angleSpd;

			m_params.recordSpeed(m_speedInput, speedOutput);
			m_params.recordPosition(m_positionInput, positionOutput);
			m_params.recordAngle(m_angleInput, angleOutput);

			Console.WriteLine();
			//Console.WriteLine(
			//	m_input.speed + ", " + m_input.angle + ", " + m_input.laneCurve + ", " + m_input.angleSpd + 
			//	" = " + m_output.angle + ", " + m_output.angleSpd);
		}

		m_speedInput = new CarParameters.SpeedInput();
		m_speedInput.speed = m_speed;
		m_speedInput.throttle = throttle;

		m_positionInput = new CarParameters.PositionInput();
		m_positionInput.speed = m_speed;
		m_positionInput.pieceIndex = myPosData.piecePosition.pieceIndex;
		m_positionInput.inPiecePosition = myPosData.piecePosition.inPieceDistance;

		m_angleInput = new CarParameters.AngleInput();
		m_angleInput.speed = m_speed;
		m_angleInput.pieceCurve = pieceCurve(myPosData.piecePosition.pieceIndex, lane);
		m_angleInput.angle = myPosData.angle;
		m_angleInput.angleSpd = m_angleSpd;

		PieceData nextPiece = m_raceData.track.pieces[pieceIndex(pieceID + 1)];

		//int nextLane = myPosData.piecePosition.lane.endLaneIndex;
		int switchLane = getLane(pieceID, lane);

		if (nextPiece.switch_&& m_nextLane != switchLane)
		{
			if(switchLane < lane )
			{
				send(new SwitchLane("Left"));
				m_nextLane = switchLane; 
			}
			else if (nextPiece.switch_ && switchLane > lane)
			{
				send(new SwitchLane("Right"));
				m_nextLane = switchLane;
			}
			else
				send(new Throttle(throttle));
		}
		else
			send(new Throttle(throttle));
		
	}

	protected override void joinRace(PlayerIDData data)
	{
		Console.WriteLine(data.name + " Joined");			
	}
	protected override void gameInit(GameInitData data)
	{
		m_raceData = data.race;

		Console.WriteLine(m_myName + " Race init " + m_raceData.track.pieces.Length);
		send(new Ping());
	}
	protected override void yourCar(PlayerData data)
	{
		Console.WriteLine("My car: " + data.name + " " + data.color);
		m_myName = data.name;
		send(new Ping());
	}

	protected override void gameEnd(object data)
	{
		Console.WriteLine(m_myName + " Race ended");
		send(new Ping());
	}
	protected override void gameStart(object data)
	{
		Console.WriteLine(m_myName + " Race starts");
		send(new Ping());
	}

	protected override void crash(PlayerData data)
	{
		if (data.name == m_myName)
		{
			Console.WriteLine(m_myName + " Crash!");
			m_crashed = true;
			m_params.crashAngle = Math.Abs(m_lastAngle);
			//Done = true;
		}
	}

	protected override void spawn(PlayerData data)
	{
		if (data.name == m_myName)
		{
			Console.WriteLine(m_myName + " Crash!");
			m_crashed = false;
		}
	}

	protected override void tournamentEnd(object data)
	{
		Console.WriteLine("End");
		Done = true;
	}

	protected override void turboAvailable(TurboData data)
	{
		Console.WriteLine("Turbo");
		//send(new Turbo("Wroom!"));
	}
	
	#endregion

	#region PRIVATE FUNCTIONS
	
	float pieceCurve(int pieceIndex, int lane)
	{
		return Utils.pieceCurve(m_raceData.track, m_raceData.track.pieces[pieceIndex], lane);
	}
	
	int piece(int startID, float piecePosition, int lane)
	{
		int pieceID = startID;
		PieceData piece = m_raceData.track.pieces[pieceID];
		float laneLength = Utils.pieceLength(m_raceData.track, piece, lane);

		while (piecePosition > laneLength)
		{
			pieceID = (++pieceID) % m_raceData.track.pieces.Length;
			piece = m_raceData.track.pieces[pieceID];
			piecePosition -= laneLength;
			laneLength = Utils.pieceLength(m_raceData.track, piece, lane);
		}

		return pieceID;
	}

	int pieceIndex(int i)
	{
		return i % m_raceData.track.pieces.Length;
	}

	int getLane(int startPieceIndex, int startLaneIndex)
	{
		int nextSwitchID = startPieceIndex;
		float angle = 0;

		while (!m_raceData.track.pieces[nextSwitchID].switch_)
		{
			nextSwitchID = pieceIndex(nextSwitchID + 1);
		}

		int nextSwitchID2 = pieceIndex(nextSwitchID + 1);
		while (!m_raceData.track.pieces[nextSwitchID2].switch_)
		{
			nextSwitchID2 = pieceIndex(nextSwitchID2 + 1);

			PieceData piece = m_raceData.track.pieces[nextSwitchID2];
			angle += piece.angle;
		}

		int lane = startLaneIndex;
		if (angle < 0)
			lane -= 1;
		if (angle > 0)
			lane += 1;
		if (lane >= m_raceData.track.lanes.Length)
			lane = m_raceData.track.lanes.Length - 1;
		if (lane < 0)
			lane = 0;
		return lane;
	}

	float getThrottle(CarPositionData myPosData)
	{
		// Test if the car would nearly flip with 0 throttle, we need to break. If it doesn't flip, throttle 1.0f.

		// If too low speed, just accelerate
		if(m_speed < 1.0f)
			return 1.0f;
		
		float testSpeed = m_speed * 1.01f;
		int lane = myPosData.piecePosition.lane.startLaneIndex;

		CarParameters.SpeedInput speedInput = new CarParameters.SpeedInput();
		speedInput.speed = testSpeed;
		speedInput.throttle = 0.0f;

		CarParameters.PositionInput positionInput = new CarParameters.PositionInput();
		positionInput.speed = testSpeed;
		positionInput.pieceIndex = myPosData.piecePosition.pieceIndex;
		positionInput.inPiecePosition = myPosData.piecePosition.inPieceDistance;

		CarParameters.AngleInput angleInput = new CarParameters.AngleInput();
		angleInput.speed = testSpeed;
		angleInput.pieceCurve = pieceCurve(myPosData.piecePosition.pieceIndex, lane);
		angleInput.angle = myPosData.angle;
		angleInput.angleSpd = m_angleSpd;

		CarParameters.SpeedOutput speedOutput;
		CarParameters.PositionOutput positionOutput;
		CarParameters.AngleOutput angleOutput;

		int i = 0;
		float totalError = 0;

		while (i < 30)
		{
			float error;
			speedOutput = m_params.getSpeed(speedInput, out error);
			totalError += error;
			positionOutput = m_params.getPosition(positionInput, out error);
			totalError += error;
			angleOutput = m_params.getAngle(angleInput, out error);
			totalError += error;
			i++;

			if (speedOutput != null)
			{

				// If angle goes too high we need to break
				if (Math.Abs(angleOutput.angle + angleOutput.angleSpd * 5) > m_params.crashAngle * .99f)
				{
					return 0.0f;
				}
				
				speedInput.speed = speedOutput.speed;

				positionInput.speed = speedOutput.speed;
				positionInput.pieceIndex = positionOutput.pieceIndex;
				positionInput.inPiecePosition = positionOutput.inPiecePosition;

				lane = getLane(lane, positionInput.pieceIndex);
				angleInput.speed = speedOutput.speed;
				angleInput.pieceCurve = pieceCurve(positionInput.pieceIndex, lane);
				angleInput.angle = angleOutput.angle;
				angleInput.angleSpd = angleOutput.angleSpd;
			}

		}
		Console.WriteLine(totalError);
		if (totalError > 100.0f)
			return 1.0f / (totalError*.01f);
		else return 1.0f;

	}

	#endregion

	#region VARIABLES

	private string m_myName;
	private RaceData m_raceData;
	private CarParameters m_params;
	private bool m_crashed;
	private float m_lastPos;
	private float m_speed;
	private float m_angleSpd;
	private float m_lastAngle;
	private int m_nextLane;
	private int m_gameTick;


	private CarParameters.SpeedInput m_speedInput;
	private CarParameters.PositionInput m_positionInput;
	private CarParameters.AngleInput m_angleInput;
	
	#endregion
}

