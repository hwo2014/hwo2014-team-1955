using System;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class MessageHandler
{
	public virtual void call(object data)
	{
	}
}
public class TMessageHandler<T> : MessageHandler
{
	protected Action<T> handler;
	public TMessageHandler(Action<T> handler)
	{
		this.handler = handler;
	}

	public override void call(object data)
	{
		if (data == null)
		{
			handler(default(T));
			return;
		}
		Type type = data.GetType();
		if(type == typeof(JArray))
			handler(((JArray)data).ToObject<T>());
		else if(type == typeof(JObject))
			handler(((JObject)data).ToObject<T>());
	}
}

public class App
{

	public static void Main(string[] args)
	{
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];

		string track = "keimola";
		string gameKey = null;
		int cars = 1;
		
		if(args.Length > 4)
			track = args[4];
		if(args.Length > 5)
			gameKey = args[5];
		if(args.Length > 6)
			cars = int.Parse(args[6]);
		Console.WriteLine(track + " " + gameKey + " " + cars);

		//while (true)
		{
			string botFile = "bot_" + track + ".json";

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			Botti bot = new Botti();
			bot.loadData(botFile);

			Thread thread = new Thread(delegate()
			{

				startBot(bot, host, port, new JoinRace(botName, botKey, track, gameKey, cars));

			});

			thread.Start();

			//while (!bot.Done)
			//{
			//}

			Console.ReadKey();

			thread.Abort();

			bot.saveData(botFile);

			Console.ReadKey();
		}

	}

	public static void startBot(Bot bot, string host, int port, SendMsg sendmsg)
	{
		using (TcpClient client = new TcpClient(host, port))
		{
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			bot.writer = writer;

			writer.WriteLine(sendmsg.ToJson());

			string line;
			while ((line = reader.ReadLine()) != null)
			{
				MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

				bot.messageReceived(msg);

				if (bot.Done)
					break;
			}

			client.Close();
		}
	}
}