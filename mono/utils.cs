using System;

public static class Utils
{
	public static float pieceLength(TrackData track, PieceData piece, int lane)
	{
		if (piece.length > 0)
			return piece.length;

		float laneOffset = track.lanes[lane].distanceFromCenter;
		
		if (piece.angle > 0)
			laneOffset = -laneOffset;

		float r = (float)piece.radius + laneOffset;

		float length = (float)r * Math.Abs(piece.angle) * (float)Math.PI / 180;
		return length;
	}
	
	public static float pieceCurve(TrackData track, PieceData piece, int lane)
	{
		if (piece.angle == 0)
			return 0;

		float laneOffset = track.lanes[lane].distanceFromCenter;
		
		if (piece.angle > 0)
			laneOffset = -laneOffset;

		float r = (float)piece.radius + laneOffset;

		return 1.0f / r;
	}

}