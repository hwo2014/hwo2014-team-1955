using System;
using System.Collections.Generic;
using System.IO;


public abstract class Bot
{
	#region PROPERTIES

	public bool Done { get; protected set; }
	
	#endregion

	#region PUBLIC
	
	public StreamWriter writer;

	public Bot()
	{
		registerHandler<CarPositionData[]>	("carPositions", 	this.carPositions);
		registerHandler<PlayerIDData>		("joinRace", 		this.joinRace);
		registerHandler<PlayerIDData>		("createRace", 		this.createRace);
		registerHandler<GameInitData>		("gameInit", 		this.gameInit);
		registerHandler<PlayerData>			("yourCar", 		this.yourCar);
		registerHandler<object>				("gameEnd", 		this.gameEnd);
		registerHandler<object>				("gameStart", 		this.gameStart);
		registerHandler<PlayerData>			("crash", 			this.crash);
		registerHandler<PlayerData>			("spawn", 			this.spawn);
		registerHandler<object>				("tournamentEnd", 	this.tournamentEnd);
		registerHandler<TurboData>			("turboAvailable",	this.turboAvailable);

		Done = false;
	}
	public void messageReceived(MsgWrapper msg)
	{
		//m_gameTick = msg.gameTick;
		if (!m_handlers.ContainsKey(msg.msgType))
			Console.WriteLine("WARNING: message type not recognized: " + msg.msgType);
		else
			m_handlers[msg.msgType].call(msg.data);

	}

	#endregion
	
	#region VIRTUAL

	protected virtual void carPositions(CarPositionData[] data) {}
	protected virtual void createRace(PlayerIDData data) {}
	protected virtual void joinRace(PlayerIDData data) {}
	protected virtual void gameInit(GameInitData data) {}
	protected virtual void yourCar(PlayerData data) {}
	protected virtual void gameStart(object data) {}
	protected virtual void gameEnd(object data) {}
	protected virtual void crash(PlayerData data) {}
	protected virtual void spawn(PlayerData data) {}
	protected virtual void tournamentEnd(object data) {}
	protected virtual void turboAvailable(TurboData data) {}
	
	#endregion

	#region PROTECTED
	
	protected void send(SendMsg msg)
	{
		writer.WriteLine(msg.ToJson());
	}
	
	#endregion
	
	#region PRIVATE
		
	private void registerHandler<T>(string msgType, System.Action<T> handler)
	{
		m_handlers[msgType]	= new TMessageHandler<T>(handler);
	}
	
	private Dictionary<string, MessageHandler> m_handlers = new Dictionary<string, MessageHandler>();

	#endregion

}